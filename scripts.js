let buttons = document.querySelectorAll(".btn");
let btn = {};
function updateButtonState(key) {
  buttons.forEach((button) => {
    button.classList.remove("active");
  });
  let selectedButton = btn[key];
  if (selectedButton) {
    selectedButton.classList.add("active");
  }
}
document.addEventListener("keydown", (event) => {
  let key = event.key.toUpperCase();
  let selectedButton = btn[key];

  if (selectedButton) {
    selectedButton.classList.remove("active");
  }
  btn[key] = Array.from(buttons).find(
    (button) => button.textContent.toUpperCase() === key
  );

  updateButtonState(key);
});

buttons.forEach((button) => {
  button.addEventListener("blur", (event) => {
    let key = event.target.textContent;
    btn[key] = null;
    updateButtonState(key);
  });
});
